


let express = require("express");
	//load express module to be able to use express properties and methods

const PORT = 4000;
	//store port 4000 in a variable

let app = express();
	//app is now our server

//middlewares
	//setup for allowing the server to handle data from requests(client);
app.use(express.json());
	//allow your app to read json data
app.use(express.urlencoded({extended:true}));
	//allow your app to read data from the forms


//routes
app.get("/", (req, res)=> res.send(`Hello World`));

//mini activity
	//create a route that will show a message "Hello from the /hello endpoint"

app.get("/hello", (req, res) => res.send(`Hello from the /hello Endpoint`));


//mini activity
	//create a greetings route that will send request body data to the server
	//display dynamic message depending on the value of the request body data
	//message should be "Hello there, Joy!"
		//if data changed, the response message should display the corresponding changed value
	//example: "Hello there, Miah!"
app.post("/greeting", (req, res) => {
	console.log(req.body);

	res.send(`Hello there ${req.body.firstname}!`);

	// console.log(`Hello there, !`)
});


app.listen(PORT, ()=> {
	console.log(`Server running at port ${PORT}`)
});